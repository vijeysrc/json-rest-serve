'use strict';

let jsonRest = require('../');

jsonRest.resource.add({
    root: 'api',
    name: 'person',
    key: '_id',
    data: require('./data/person.json'),
    pageSize: 10
});

jsonRest.resource.add({
    root: 'rest',
    name: 'country',
    key: 'code',
    data: require('./data/country.json'),
    pageSize: 25,
    readOnly: true
});

jsonRest.resource.add({
    root: 'api',
    name: 'employee',
    key: 'id',
    data: require('./data/employee.json'),
    pageSize: 8
});

jsonRest.serve(9000);