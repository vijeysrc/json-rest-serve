# JSON REST Serve
A NodeJs tool to serve a JSON data as REST end points.

## Install

```sh
$ npm install json-rest-serve
```

##Requirements

* The JSON data should be an array of objects.
* Each object (data item) should be identifiable with a primary key property.
* The primary key should be either string or integer.
* The data item can have nested objects within it.
* The added resource should have atleast one data item in its data array.
* The tool uses the first data item to understand the structure of it.  It then uses this information to enforce the same structure during the POST and PUT operations.

## How to use
**Steps:**

* Add one or more resources that you want to serve as REST endpoints.
* After adding, call the serve method with your preferred port number.
* name, key and data are required to add a resource.

```js
'use strict';

let jsonRestServe = require('json-rest-serve');

jsonRestServe.resource.add({
    root: 'api',
    name: 'employee',
    key: 'id',
    data: require('./data/employee.json'),
    pageSize: 8
});

jsonRestServe.resource.add({
    root: 'static',
    name: 'country',
    key: 'code',
    data: require('./data/country.json'),
    pageSize: 25,
    readOnly: true
});

jsonRestServe.serve(9000);
```

**Outcome**

* REST endpoints for employee such as http://localhost:9000/api/employee, http://localhost:9000/api/employee/:id
* REST endpoints for country such as http://localhost:9000/static/country, http://localhost:9000/static/country/:code
* You can do all the REST CRUD operations on employee resource.
* You can do only read and read all operation on country resource as it is marked as readOnly.
* You can view the document pages for the added resources at http://localhost:9000/documents/index.html
* The first data item from each resource's data array can be seen as a sample data in the above document link.
* Please see the examples directory for the complete code and data.

## Options and Parameters

### resource.add(options)
Key | Type | Default | Description |
--- | --- | --- | --- |
root|String|api|Forms the base of rest urls after http://localhost:9000|
name|String|Required field|This would be the name of the resource|
key|String|Required field|Name of the primary key attribute of the data item.|
version|String||This version value would be added to the url. Example: http://localhost:9000/v1/api/employee|
data|Array|Required field|An array of objects with a definite primary key|
pageSize|Integer|10|Used in get all operation which is paginated.|
schema|Object|The tool generates a schema when not provided|You can provide a robust schema for your JSON data item. The schema should adhere to draft-04 of json-schema.org|


### serve(portNumber, morganLogFormat)
Parameter | Type | Default | Description |
--- | --- | --- | --- |
portNumber|Integer|9000|Port number of the REST endpoints|
morganLogFormat|String|dev|Takes pre-defined formats provided by Morgan logging tool such as combined, common, dev, short, tiny, etc.|


## Features

* Can serve any array of objects JSON data with any structure and with any primary key.
* Can add as many resources before calling serve method.
* Does not update the json file for PUT/POST/DELETE operations. All the changes you make through the REST calls will be lost once you terminate the process.
* CORS enabled
* Can be used as a developer tool and not for any production deployment.
* Filter, sort, search and field selection features are available with GET requests.  See the generated apidocs (http:<url>/documents/index.html) for more information.

## Driven by

* Primarily powered by Lodash and Restify
* Supported by other libraries such as Morgan, Ajv and Rimraf.
* Document site is powered by apidoc.

## License

MIT
