'use strict';

let fs = require('fs');
let path = require('path');
let restify = require('restify');
let apidoc = require('apidoc');
let rimraf = require('rimraf');
let _ = require('lodash');
let resourceManager = require('./resource-manager');

let tmpDir = path.join(process.cwd(), '.tmp-jrs');
let tmpDirJs = path.join(tmpDir, 'js');
let tmpDirHtml = path.join(tmpDir, 'documents');

rimraf.sync(tmpDir);

!fs.existsSync(tmpDir) && fs.mkdirSync(tmpDir);
!fs.existsSync(tmpDirJs) && fs.mkdirSync(tmpDirJs);
!fs.existsSync(tmpDirHtml) && fs.mkdirSync(tmpDirHtml);

let serve = (server) => {
  resourceManager.all().forEach((resource) => {
    const {data, name, key, schema, isIntKey, baseUrl, baseParamUrl, readOnly} = resource;
    const tmpFileJs = path.join(tmpDirJs, name + '.js');

    const ops = readOnly ? ['common', 'get', 'all'] : ['common', 'get', 'all', 'post', 'put', 'remove'];

    let getApiSuccess = (properties, base) => {
      let content = (value, key, base) => ' * @apiSuccess {' + _.capitalize(value.type) + '} ' + (base ? base + '.' + key : key);

      return _.flattenDeep(_.map(properties, (value, key) => {
        if (value.type === 'object') {
          return [content(value, key, base)].concat(getApiSuccess(value.properties, key));
        } else {
          return content(value, key, base);
        }
      }));

    };
    let getApiSuccessExample = () => '* @apiSuccessExample Success-Response: \n HTTP/1.1 200 OK \n' + JSON.stringify(data[0], null, 2);

    let docs = {};

    docs.common = `
      /**
       * @apiDefine NotFoundError
       *
       * @apiError NotFoundError No resource found for the key: ${key} on ${_.capitalize(name)}
       *
       */

       /**
        * @apiDefine BadRequestError
        *
        * @apiError BadRequestError Input data is not valid as per schema
        *
        */
    `;

    docs.get = `
      /**
       * @api {get} ${baseParamUrl} 1. Read
       * @apiName Get${_.capitalize(name)}
       * @apiGroup ${_.capitalize(name)}
       *
       * @apiParam {${isIntKey ? 'Number' : 'String'}} ${key} unique ID of ${_.capitalize(name)}.
       * @apiParam (Query Strings) {Number} page Page number.
       * @apiParam (Query Strings) {String} fields Comma seperated field names to specify the output structure; Ex. fields=age,salary,name.
       *
       ${getApiSuccess(schema.properties).join('\n')}
       ${getApiSuccessExample()}
       *
       * @apiUse NotFoundError
       *
       */
    `;

    docs.all = `
      /**
       * @api {get} ${baseUrl} 2. Read All
       * @apiName GetAll${_.capitalize(name)}
       * @apiGroup ${_.capitalize(name)}
       *
       * @apiParam {${isIntKey ? 'Number' : 'String'}} ${key} unique ID of ${_.capitalize(name)}.
       * @apiParam (Query Strings) {Number} page Page number.
       * @apiParam (Query Strings) {String} fields Comma seperated field names to specify the output structure; Ex. fields=age,salary,name.
       * @apiParam (Query Strings) {String} sort Comma seperated field names to sort by multiple fields; sort=-salary,name.
       * @apiParam (Query Strings) {String} field-name To apply filter based on a field value; Ex. age=25.
       *
       *
       * @apiUse NotFoundError
       *
       */
    `;

    docs.post = `
      /**
       * @api {post} ${baseUrl} 3. Create
       * @apiName Post${_.capitalize(name)}
       * @apiGroup ${_.capitalize(name)}
       *
       * @apiUse BadRequestError
       */
    `;

    docs.put = `
      /**
       * @api {put} ${baseParamUrl} 4. Update
       * @apiName Put${_.capitalize(name)}
       * @apiGroup ${_.capitalize(name)}
       *
       * @apiParam {${isIntKey ? 'Number' : 'String'}} ${key} unique ID of ${_.capitalize(name)}.
       *
       * @apiUse NotFoundError
       * @apiUse BadRequestError
       *
       */
    `;

    docs.remove = `
      /**
       * @api {delete} ${baseParamUrl} 5. Delete
       * @apiName Delete${_.capitalize(name)}
       * @apiGroup ${_.capitalize(name)}
       *
       * @apiParam {${isIntKey ? 'Number' : 'String'}} ${key} unique ID of ${_.capitalize(name)}.
       *
       * @apiUse NotFoundError
       *
       */
    `;

    fs.writeFileSync(tmpFileJs, _.chain(ops)
      .map((op) => docs[op])
      .join()
      .split('\n')
      .map((line) => line.replace(/^\s+/, ' ').replace(/^\*/, ' *'))
      .join('\n')
      .value()
    );
  });

  apidoc.createDoc({
    silent: true,
    src: tmpDirJs,
    dest: tmpDirHtml
  });

  server.get(/\/documents\/?.*/, restify.serveStatic({
    directory: tmpDir,
    default: 'index.html'
  }));
};

module.exports = {
  serve: serve
};