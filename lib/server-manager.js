'use strict';

let restify  = require('restify');
let resourceManager = require('./resource-manager');
let routeManager = require('./route-manager');
let documentManager = require('./document-manager');
let logger = require('morgan');

let serve = (port, morganLogFormat) => {
  let server;
  let ready = resourceManager.isReady();

  if (ready !== true) {
    console.log(ready + '...exiting!!!!');
    return;
  }

  port = port || 9000;

  server = restify.createServer();

  server.use(logger(morganLogFormat || 'dev', {
    skip: (req) => /^\/documents/.test(req.url)
  }));
  server.use(restify.CORS());
  server.use(restify.gzipResponse());
  server.use(restify.bodyParser());
  server.use(restify.queryParser());

  routeManager.setup(server);
  documentManager.serve(server);

  server.listen(port, function () {
      console.log('REST server is now available at port number: ' + port);
      console.log('Serving...', resourceManager.names().join(', '))
  });
};

module.exports = {
  serve: serve
};