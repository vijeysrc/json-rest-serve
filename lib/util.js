'use strict';

let _ = require('lodash');
let Ajv = require('ajv');

const constants = {
  $schema: 'http://json-schema.org/draft-04/schema#',
  resourceProperties: {
    root: 'string',
    version: 'string',
    name: 'string',
    key: 'string',
    pageSize: 'integer',
    data: 'array',
    readOnly: 'boolean',
    schema: 'object'
  },
  typeFns: [
    ['isPlainObject', 'object'],
    ['isArray', 'array'],
    ['isBoolean', 'boolean'],
    ['isString', 'string'],
    ['isNumber', 'number'],
    ['isNull', 'null']
  ]
};

let getResourceSchema = () => {
  return {
    $schema: constants.$schema,
    title: 'Resource Schema',
    type: 'object',
    required: ['name', 'key', 'data'],
    properties: _.reduce(constants.resourceProperties, (output, value, key) => { output[key] = {type: value}; return output }, {})
  };
};

let validate = (schema, data) => {
  let ajv = new Ajv();
  let status = ajv.validate(schema, data);

  return {
    status: status,
    errors: ajv.errors
  };
};

let validateWithResourceSchema = (resource) => validate(getResourceSchema(), resource);

let type = (value) => _.chain(constants.typeFns).find(fn => _[fn[0]].call(null, value)).thru(arr => _.isArray(arr) ? arr[1] : 'unknown').value();

let parse = (data) => {
  return _.reduce(data, (result, value, key) => {
    if (_.isPlainObject(value)) {
      result[key] = {
        type: 'object',
        properties: parse(value)
      };
    } else {
      result[key] = {
        type: type(value)
      }
    }

    return result;
  }, {});
};

let jsonToSchema = (name, key, data) => {
  let schema = {
    $schema: constants.$schema,
    required: [key]
  };
  schema.title = _.capitalize(name) + ' Schema';
  schema.properties = parse(data);
  return schema;
};

module.exports = {
  validate: validate,
  validateWithResourceSchema: validateWithResourceSchema,
  jsonToSchema: jsonToSchema
};