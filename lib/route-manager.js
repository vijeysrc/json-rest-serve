'use strict';

let restify = require('restify');
let _ = require('lodash');

let resourceManager = require('./resource-manager');
let util = require('./util');

let setup = (server) => {
  let resources = resourceManager.all();

  resources.forEach(resource => {
    let {key, data, name, readOnly, schema, pageSize, baseUrl, baseParamUrl, isIntKey} = resource;

    let getPayload = (req) => {
      let output;

      try {
        output = _.isString(req.body) ? JSON.parse(req.body) : req.body;
        output[key] = pkey(req);
        return output;
      } catch (err) {
        return {};
      }
    };
    let pkey = (req) => schema.properties[key].type === 'number' ? +req.params[key] : req.params[key];
    let getDataIndex = (req) => _.findIndex(data, (item) => item[key] === pkey(req));
    let getDataItem = (req) => _.find(data, (item) => item[key] === pkey(req));

    let messages = [
      _.template('<%= operation %> operation is not allowed on <%= name %>'),             //0
      _.template('Incorrect data sent for <%= operation %> on <%= name %>'),              //1
      _.template('Data item already present in database.'),                               //2
      _.template('No resource found for the key: <%= pkey %> on <%= name %>'),            //3
      _.template('Input data is not valid as per schema'),                                //4
      _.template('<%= operation %> operation on <%= name %> completed successfully')      //5
    ];
    let getMessage = (config) => messages[config.messageId](config);
    let getSuccessMessage = (config) => ({code: 'success', 'message': getMessage(config)});
    let getRestifyError = (config) => new restify.errors[config.type + 'Error'](getMessage(config));

    let runChecks = (req, checkList) => {
      let checks = {
        readOnly:  () => readOnly ? {error: getRestifyError({type: 'MethodNotAllowed', messageId: 0, operation: 'create/update/delete', name: name})} : {},
        payload:   () => _.isEmpty(getPayload(req)) ? {error: getRestifyError({type: 'BadRequest', messageId: 1, operation: 'update', name: name})} : {payload: getPayload(req)},
        found:     () => (getDataIndex(req) !== -1) ? {error: getRestifyError({type: 'BadRequest', messageId: 2})} : {index: getDataIndex(req)},
        notFound:  () => (getDataIndex(req) === -1) ? {error: getRestifyError({type: 'NotFound', messageId: 3, pkey: pkey(req), name: name})} : {index: getDataIndex(req)},
        isValid:   () => {
          let validate = util.validate(schema, _.merge(_.cloneDeep(getDataItem(req)), getPayload(req)));
          return validate.status === false ? {error: getRestifyError({type: 'BadRequest', messageId: 4})} : {};
        }
      };

      return _.reduce(checkList, (result, fn) => {
        return result.error ? result : _.merge(result, checks[fn].call(null));
      }, {});
    };

    let keySeed = 0;
    let maxKey = () => _.max(_.map(data, key));
    let getPrimaryKey = () => isIntKey ? (maxKey() + 1) : (name + '-' + ++keySeed);

    let getChosenOptions = (req, key) => _.isString(req.query[key]) ? _.map(req.query[key].split(','), (item) => _.trim(item)) : [];

    server.get(baseUrl, (req, res) => {
      let page = /^[0-9]+$/.test(req.query.page) ? +req.query.page : 1;
      let start = (page - 1) * pageSize;
      let sort = getChosenOptions(req, 'sort');
      let ascDesc = _.map(sort, (item) => /^\-/.test(item) ? 'desc' : 'asc');
      let fields = getChosenOptions(req, 'fields');

      sort = _.map(sort, (item) => item.replace(/^-/, ''));
      _.forEach(_.omit(req.query, ['page', 'fields', 'sort', 'q']), (value, key) => req.query[key] = (schema.properties[key].type === 'number') ? +value : value);

      let result = _.chain(data)
        .filter(_.omit(req.query, ['page', 'fields', 'sort', 'q']))
        .orderBy(sort, ascDesc)
        .map((item) => fields.length ? _.pick(item, fields) : item)
        .filter((item) => req.query.q ? (new RegExp(req.query.q, 'i')).test(JSON.stringify(item)) : true)
        .value();

      res.send({
        code: 'success',
        page: page,
        dataCount: result.length,
        pageSize: pageSize,
        payload: _.slice(result, start, start + pageSize)
      });
    });

    server.get(baseParamUrl, (req, res) => {
      let chosenFields = getChosenOptions(req, 'fields');

      res.send(
        _.chain(data)
          .find((item) => item[key] === pkey(req))
          .thru((item) => chosenFields.length ? _.pick(item, chosenFields) : item)
          .thru((result) => result ? ({status: 'success', payload: result}) : getRestifyError({type: 'NotFound', messageId: 3, pkey: pkey(req), name: name}))
          .value()
      );
    });

    server.post(baseUrl, (req, res) => {
      req.params[key] = getPrimaryKey();

      let result = runChecks(req, ['readOnly', 'payload', 'found', 'isValid']);
      let add = (result) => data.push(result.payload) && getSuccessMessage({ messageId: 5, operation: 'add', name: name});
      let done = (result) => result.error ? result.error : add(result);

      res.send(
        done(result)
      );
    });

    server.put(baseParamUrl, (req, res) => {
      let result = runChecks(req, ['readOnly', 'payload', 'notFound', 'isValid']);
      let update = (result) => _.merge(data[getDataIndex(req)], result.payload) && getSuccessMessage({ messageId: 5, operation: 'update', name: name});
      let done = (result) => result.error ? result.error : update(result);

      res.send(
        done(result)
      );
    });

    server.del(baseParamUrl, (req, res) => {
      let result = runChecks(req, ['readOnly', 'notFound']);
      let remove = (result) => _.remove(data, (item, index) => result.index === index) && getSuccessMessage({ messageId: 5, operation: 'delete', name: name});
      let done = (result) => result.error ? result.error : remove(result);

      res.send(
        done(result)
      );
    });
  });
};

module.exports = {
  setup: setup
};