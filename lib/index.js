'use strict';

let path = require('path');
let rimraf = require('rimraf');

let resourceManager = require('./resource-manager');
let serverManager = require('./server-manager');

let tmpDir = path.join(process.cwd(), '.tmp-jrs');

let cleanup = () => {
  rimraf(tmpDir, () => {
    process.exit();
  });
};

process.on('SIGINT', cleanup);

module.exports = {
  resource: {
    add: resourceManager.init
  },
  serve: serverManager.serve
};