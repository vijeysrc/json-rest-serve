'use strict';

let _ = require('lodash');
let util = require('./util');

const constants = {
  db: [],
  initClean: true
};

let init = (input) => {
  let resource = {
    root: input.root || 'api',
    version: input.version,
    name: input.name,
    key: input.key,
    pageSize: input.pageSize || 10,
    data: input.data,
    readOnly: input.readOnly || false,
    schema: input.schema || {}
  };

  resource.baseUrl = '/' + resource.root + (resource.version ? '/' + resource.version + '/'  : '/') + resource.name;
  resource.baseParamUrl = resource.baseUrl + '/:' + resource.key;
  resource.isIntKey = _.isInteger(resource.data[0][resource.key]);

  let check = util.validateWithResourceSchema(resource);

  if (check.status) {
    if (_.isEmpty(resource.schema)) {
      resource.schema = util.jsonToSchema(resource.name, resource.key, resource.data[0]);
    }
    constants.db.push(resource);
  } else {
    constants.initClean = false;
    console.log('Error initializing ' + resource.name + ': ');
    check.errors.forEach(error => console.log(error.message));
  }
};

let isReady = () => {
  if (constants.db.length === 0) {
    return 'No resources to serve';
  }

  if (!constants.initClean) {
    return 'Not a clean init';
  }

  return true;
};

module.exports = {
  init: init,
  all: () => constants.db,
  isReady: isReady,
  names: () => _.map(constants.db, 'name')
};